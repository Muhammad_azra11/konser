<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\IndexController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', [IndexController::class, 'index']);
Route::get('pesan', [IndexController::class, 'pesan']);
Route::post('pesan', [IndexController::class, 'store']);
Route::get('filter', [HomeController::class, 'index']);
Route::get('edit/{id}', [HomeController::class, 'edit']);
Route::put('edit/{id}', [HomeController::class, 'update'])->name('edit.update');
Route::get('delete/{id}', [HomeController::class, 'delete']);
Route::get('input', [HomeController::class, 'input']);
Route::get('cari', [HomeController::class, 'cari']);
// Route::post('cari/{id}', [HomeController::class, 'cari']);
// Route::get('cari', [HomeController::class, 'cari']);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
