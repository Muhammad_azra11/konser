<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('index', function (Blueprint $table) {
            $table->id();
            $table->string('nama');
            $table->char('nomor_hp');
            $table->string('email');
            $table->string('jenis_tiket');
            $table->string('jumlah_tiket');
            $table->string('harga_tiket');
            $table->date('tanggal_konser');
            $table->enum('status', ['aktif', 'sudah_dipakai']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('index');
    }
};
