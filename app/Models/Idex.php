<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Idex extends Model
{
    use HasFactory;
    protected $fillable = [
        'nama',
        'nomor_hp',
        'email',
        'jenis_tiket',
        'jumlah_tiket',
        'harga_tiket',
        'tanggal_konser',
    ];
}
