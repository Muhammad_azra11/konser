<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Staf extends Model
{
    use HasFactory;
    protected $table = 'staf';
    protected $primaryKey = 'id_staf';
    protected $fillable = [
        'nama_staf', 'username', 'password'
    ];
}
