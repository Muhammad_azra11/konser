<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function index() {
        return view('auth.login_staf');
    }

    public function hadleLoginPetugas(Request $request) 
    {
        $request->validate([
            'username' => 'required',
            'password' => 'required'
        ]);
        if(Auth::guard('petugas')->attempt(['username' => $request->username,'password' => $request->password])){
            return redirect('/home/petugas');
        }else{
            return redirect()->back()->with('danger', 'Username atau Password anda Salah');
        }
    }
}
