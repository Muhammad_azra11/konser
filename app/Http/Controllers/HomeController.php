<?php

namespace App\Http\Controllers;

use App\Models\Index;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $data = Index::all();
        if($request->filter){
            $data = Index::where('status', $request->filter)->get();
        }
        return view('staf.index', compact('data'));
        // $data = Index::all();
    }

    public function edit($id)
    {
        $edit = Index::find($id);
        return view('staf.edit', compact('edit'));
    }

    public function update(Request $request, $id)
    {
        $edit = Index::find($id);
        $edit->update($request->all());

        return redirect()->route('home');
    }

    public function delete($id)
    {
        $edit = Index::find($id);
        $edit->delete();
        return redirect()->route('home');
    }

    public function input()
    {
        $data = Index::all();
        return view('staf.input', compact('data'));
    }
    
    public function cari(Request $request)
    {

        $data = Index::all();
        $isi = 'SILAHKAN MASUK';
        if($request->cari) {
            $data = Index::where('id', $request->cari)->get();
             Index::where(['id' => $request->cari])->update(['status' => 'sudah_dipakai']);
        }else if (Index::where(['status' => 'sudah_dipakai'])){
            return null;
        }
        // $cari = $request->cari;

        // $data = DB::table('index')
		// ->where('id','like',"%".$cari."%")
		// ->paginate();
        // $data->update(['status' => 'sudah_dipakai']);
        // if($data){
        //     Index::where('status', '=', 'status')->update(['status' => 'sudah_dipakai']);
        // }
        // Index::where('id', )->update(['status' => 'sudah_dipakai']);
        // $data = Index::where('id', 'like', "%".$cari."%");
        return view('staf.input', compact('data', 'isi'));
    }
    
    // public function filter(Request $request) 
    // {
    //     $index = Index::all();
    //     if($request->filter){
    //         $index = Index::where('status', $request->filter)->get();

    //         return 
    //     }
    // }
}
