<?php

namespace App\Http\Controllers;

use App\Models\Idex;
use App\Models\Index;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index() 
    {
        return view('penonton.index');
    }

    public function pesan(){
        $pesan = Index::all();
        return view('penonton.pesan', compact('pesan'));
    }

    public function store(Request $request)
    {
        
       Index::create([
       'nama' => $request->nama,
       'nomor_hp' => $request->nomor_hp,
       'email' => $request->email,
       'jenis_tiket' => $request->jenis_tiket,
       'jumlah_tiket' => $request->jumlah_tiket,
       'harga_tiket' => $request->harga_tiket,
       'tanggal_konser' => $request->tanggal_konser,
       'status' => 'aktif',
       ]);

       
       return redirect()->back();
    }
}
