<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Business Casual - Start Bootstrap Theme</title>
        <!-- CSS -->
<style>
    .judul {
    /* font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif; */
    font-size: 50px;
    width: 20em;
    color: #ccc;
    /* padding: 1em; */
    /* border: 1px solid #ccc; */
    /* margin-bottom: 3px; */
    padding-left: 470px;
    }

.myForm {
font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
font-size: 0.8em;
width: 20em;
padding: 1em;
/* border: 1px solid #ccc; */
margin-bottom: 30px;
padding-left: 500px;
}

.myForm * {
box-sizing: border-box;
}

.myForm fieldset {
border: none;
padding: 0;
}

.myForm legend,
.myForm label {
padding: 0;
font-weight: bold;
}

.myForm label.choice {
font-size: 0.9em;
font-weight: normal;
}

.myForm input[type="text"],
.myForm input[type="tel"],
.myForm input[type="email"],
.myForm input[type="datetime-local"],
.myForm input[type="number"],
.myForm select,
.myForm textarea {
display: block;
width: 100%;
border: 1px solid #ccc;
font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
font-size: 0.9em;
padding: 0.3em;
}

.myForm textarea {
height: 100px;
}

.btnn{
  border: 1px solid black;
  padding: 5px;
  border-radius: 5px;
  text-decoration: none;
}

.myForm button {
padding: 1em;
border-radius: 0.5em;
background: #eee;
border: none;
font-weight: bold;
margin-top: 1em;
}

.myForm button:hover {
background: #ccc;
cursor: pointer;
}

table {
  font-family: arial, sans-serif;
  padding-top: 50px;
  margin-left: 50px;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
  color: #ccc;
}

tr:nth-child(even) {
  /* background-color: #dddddd; */
}

h2{
    font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
    font-size: 20px;
    width: 20em;
    padding: 1em;
    /* border: 1px solid #ccc; */
    margin-bottom: 3px;
    padding-left: 500px;
}

.full {
  position: relative;
  margin-right: 38px; 
}

.full-content{
  display: none;
  position: absolute;
  z-index: 1;
}

.full-content > a {
  display: block;
  border: 1px solid black;
  padding: 4px;
  text-decoration: none;
  margin-top: 2px;
  color: black;
  background: #0d6ad7; 
  border-radius: 5px;
}

.full:hover .full-content{
  display: flex;
}

.fullbtn {
 background: none;
 margin-right: 24px; 
 border: 1px solid black;
 padding: 3px;
 color: #dddddd;
 box-shadow: 0 0 2px 2px #fff;
}

.success {
  position: relative;
  background-color: #00CC00;
  border-radius: 20px;
  padding: 20px;
  text-align: center;
  font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
  color: #dddddd;
  font-size: 15px;
}

.ini {
  margin-left: 60px;
  margin-bottom: 11px;
}

form > .f {
  border: 1px solid white;
  padding: 5px;
  background: none;
  color: #ccc;
}

form .cari {
  border: 1px solid white;
  padding: 5px;
  background: none;
  color: #ccc;
  margin-left: 3px;
}

form > select > option {
  border: none;
  color: black;
  background-color: none;
}


</style>

        <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
        <!-- Google fonts-->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i" rel="stylesheet" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="{{asset('konser/css/styles.css')}}" rel="stylesheet" />
    </head>
    <body>
        {{-- <header>
            <h1 class="site-heading text-center text-faded d-none d-lg-block">
                <span class="site-heading-upper text-primary mb-3">A Free Bootstrap Business Theme</span>
                <span class="site-heading-lower">Business Casual</span>
            </h1>
        </header> --}}
        <!-- Navigation-->
        <nav class="navbar navbar-expand-lg navbar-dark py-lg-4" id="mainNav">
            <div class="container">
                <a class="navbar-brand text-uppercase fw-bold d-lg-none" href="index.html">Start Bootstrap</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mx-auto">
                        <li class="nav-item px-lg-4"><a class="nav-link text-uppercase" href="home">Home</a></li>
                        <li class="nav-item px-lg-4"><a class="nav-link text-uppercase" href="input">Check In</a></li>
                        <li class="nav-item px-lg-4"><a class="nav-link text-uppercase" href="{{ route('logout') }}" onclick="event.preventDefault();
                          document.getElementById('logout-form').submit();">Logout</a></li>
                          <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                              @csrf
                          </form>
                      </div>
                        {{-- <li class="nav-item px-lg-4"><a class="nav-link text-uppercase" href="products.html">Products</a></li>
                        <li class="nav-item px-lg-4"><a class="nav-link text-uppercase" href="pesan">Pesan</a></li> --}}
                    </ul>
                </div>
            </div>
        </nav>
        <h2 class="judul">Data Pengunjung</h2>
        <form action="filter" class="ini">
          <select name="filter" class="f" id="">
            <option value="aktif" @if(request()->filter == 'aktif') selected @endif>Aktif</option>
            <option value="sudah_dipakai" @if(request()->filter == 'sudah_dipakai') selected @endif>Sudah Dipakai</option>
          </select>
          <button class="cari">Cari</button>
        </form>
<table>
  <thead>
  <tr>
    <th>Nama</th>
    <th>Nomor Hp</th>
      <th>Email</th>
      <th>Jenis Tiket</th>
      <th>JUmlah Tiket</th>
      <th>Harga Tiket</th>
      <th>Tanggal Konser</th>
      <th>Status</th>
      <th>Option</th>
    </tr>
  </thead>
  {{-- <tbody> --}}
    @foreach($data as $d)
    <tr>
      
      <td>{{$d->nama}}</td>
      <td>{{$d->nomor_hp}}</td>
      <td>{{$d->email}}</td>
      <td>{{$d->jenis_tiket}}</td>
      <td>{{$d->jumlah_tiket}}</td>
      <td>{{$d->harga_tiket}}</td>
      <td>{{$d->tanggal_konser}}</td>
      <td>{{$d->status}}</td>
      <td>
        <div  class="full">
        <button class="fullbtn">
          Option
        </button>
        <div class="full-content">
          <a href="edit/{{$d->id}}">Edit</a>
          <a href="delete/{{$d->id}}">Hapus</a>
        </td>
    </div>
      </div>
    </tr>
  {{-- </tbody> --}}
    {{-- <tr>
      <td>nn</td>
      <td>Francisco Chang</td>
      <td>Mexico</td>
    </tr>
    <tr>
      <td>Ernst Handel</td>
      <td>Roland Mendel</td>
      <td>Austria</td>
    </tr>
    <tr>
      <td>Island Trading</td>
      <td>Helen Bennett</td>
      <td>UK</td>
    </tr>
    <tr>
      <td>LWinecellars</td>
      <td>Yoshi Tannamuri</td>
      <td>Canada</td>
    </tr>
    <tr>
      <td>Riuniti</td>
      <td>Giovanni Rovelli</td>
      <td>Italy</td>
    </tr> --}}
    @endforeach
  </table>
        {{-- <section class="page-section clearfix">
            <div class="container">
                <div class="intro">
                    <img class="intro-img img-fluid mb-3 mb-lg-0 rounded" src="{{asset('konser/assets/img/intro.jpg')}}" alt="..." />
                    <div class="intro-text left-0 text-center bg-faded p-5 rounded">
                        <h2 class="section-heading mb-4">
                            <span class="section-heading-upper">Fresh Coffee</span>
                            <span class="section-heading-lower">Worth Drinking</span>
                        </h2>
                        <p class="mb-3">Every cup of our quality artisan coffee starts with locally sourced, hand picked ingredients. Once you try it, our coffee will be a blissful addition to your everyday morning routine - we guarantee it!</p>
                        <div class="intro-button mx-auto"><a class="btn btn-primary btn-xl" href="#!">Visit Us Today!</a></div>
                    </div>
                </div>
            </div>
        </section> --}}
        {{-- <section class="page-section cta"> --}}
            {{-- <div class="container">
                <div class="row">
                    <div class="col-xl-9 mx-auto">
                        <div class="cta-inner bg-faded text-center rounded">
                            <h2 class="section-heading mb-4">
                                <span class="section-heading-upper">Our Promise</span>
                                <span class="section-heading-lower">To You</span>
                            </h2>
                            <p class="mb-0">When you walk into our shop to start your day, we are dedicated to providing you with friendly service, a welcoming atmosphere, and above all else, excellent products made with the highest quality ingredients. If you are not satisfied, please let us know and we will do whatever we can to make things right!</p>
                        </div>
                    </div>
                </div>
            </div> --}}
        {{-- </section> --}}
        {{-- <footer class="footer text-faded text-center py-5">
            <div class="container"><p class="m-0 small">Copyright &copy; Your Website 2023</p></div>
        </footer> --}}
        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        <script src="{{asset('konser/js/scripts.js')}}"></script>
    </body>
</html>
