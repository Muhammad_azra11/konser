<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Data Posts - SantriKoding.com</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" />
    <style>
        .i {
            border: none;
            margin-bottom: 10px; 
        }
    </style>
</head>
<body style="background: lightgray">

    <div class="container mt-5">
        <div class="row">
            <div class="col-md-12">
                <div class="card border-0 shadow rounded">
                    <div class="card-body">
                        <form action="cari" method="GET" class="i">
                            <input type="text" name="cari" placeholder="Cari Pegawai .." value="{{ old('cari') }}">
                            <input type="submit" class="kanan" value="INPUT">
                        </form>
                        <a href="home" class="btn btn-sm btn-dark mb-2">Back</a>
                        {{-- <a href="{{ route('posts.create') }}" class="btn btn-md btn-success mb-3">TAMBAH POST</a> --}}
                        <table class="table table-bordered">
                            <thead>
                              <tr >
                                <th scope="col">Nama</th>
                                <th scope="col">Nomor Hp</th>
                                <th scope="col">Email</th>
                                <th scope="col">Jenis Tiket</th>
                                <th scope="col">JUmlah Tiket</th>
                                <th scope="col">Harga Tiket</th>
                                <th scope="col">status</th>
                                <th scope="col">Tanggal Konser</th>
                                {{-- <th style="width: 15%">AKSI</th> --}}
                              </tr>
                            </thead>
                            <tbody>
                              @forelse ($data as $post)
                              @if ($post->status == 'aktif') 
                              <div class="alert alert-success" @if (($post->status == 'aktif'))@endif>Pengunjung dengan &nbsp;&nbsp;&nbsp;&nbsp; id {{$post->id}}&nbsp;&nbsp;&nbsp;&nbsp;Silahkan masuk</div>
                                <tr >
                                    {{-- <td class="text-center">
                                        <img src="{{ asset('storage/posts/'.$post->image) }}" class="rounded" style="width: 150px">
                                    </td> --}}
                                    
                                    <td >{{ $post->nama }}</td>
                                    <td>{{ $post->nomor_hp }}</td>
                                    <td>{{ $post->email }}</td>
                                    <td>{{ $post->jenis_tiket }}</td>
                                    <td>{{ $post->jumlah_tiket }}</td>
                                    <td>{{ $post->harga_tiket }}</td>
                                    <td>{{ $post->status }}</td>
                                    <td>{{ $post->tanggal_konser }}</td>
                                    {{-- <td>{!! $post->content !!}</td> --}}
                                    {{-- <td class="text-center"> --}}
                                        {{-- <form onsubmit="return confirm('Apakah Anda Yakin ?');" action="{{ route('posts.destroy', $post->id) }}" method="POST"> --}}
                                            {{-- <a href="{{ route('posts.show', $post->id) }}" class="btn btn-sm btn-dark"><i class="fa fa-eye"></i></a>
                                            <a href="{{ route('posts.edit', $post->id) }}" class="btn btn-sm btn-primary"><i class="fa fa-pencil-alt"></i></a> --}}
                                            {{-- @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>
                                        </form>
                                    </td> --}}
                                </tr>
                                @elseif($post->status == 'sudah_dipakai')
                              <div class="alert alert-danger">tiket tidak aktif dengan &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;id {{$post->id}}</div>
                                @endif
                              @empty
                                  <div class="alert alert-danger">
                                      Tiket Tidak Tersedia.
                                  </div>
                              @endforelse
                            </tbody>
                          </table>  
                          {{-- {{ $posts->links() }} --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

    <script>
        //message with toastr
        @if(session()->has('success'))
        
            toastr.success('{{ session('success') }}', 'BERHASIL!'); 

        @elseif(session()->has('error'))

            toastr.error('{{ session('error') }}', 'GAGAL!'); 
            
        @endif
    </script>

</body>
</html>